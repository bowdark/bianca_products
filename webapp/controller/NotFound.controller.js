sap.ui.define([
	"com/bowdark/productsDemoApp/controller/BaseController" // Base Controller for back button navigation
], function(BaseController) {
	"use strict";

	return BaseController.extend("com.bowdark.productsDemoApp.controller.NotFound", {
		init: function() {
			var oRouter, oTarget;
			
			oRouter = this.getRouter();
			oTarget = oRouter.getTarget("notFound");
			oTarget.attachDisplay(function (oEvent) {
				this._oData = oEvent.getParameter("data"); // Storing the data into internal controller variable _oData
			}, this);
		},
		
		// Override the parent's onNavBack (from BaseController)
		onNavBack : function (oEvent) {
			var oHistory, sPreviousHash, oRouter;
			
			// In some cases, we could display a certain target when the back button is pressed.
			if (this._oData && this._oData.fromTarget) {
				this.getRouter().getTargets().display(this._oData.fromTarget);
				delete this._oData.fromTarget;
				return;
			}
			
			// Call the parent's onNavBack
			BaseController.prototype.onNavBack.apply(this, arguments);
		}
	});
});