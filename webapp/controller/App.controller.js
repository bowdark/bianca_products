sap.ui.define([
	"com/bowdark/productsDemoApp/controller/BaseController" // Base Controller for back button navigation
], function(BaseController) {
	"use strict";

	return BaseController.extend("com.bowdark.productsDemoApp.controller.App", {
		onInit: function() {
			
		}
		
	});
});