sap.ui.define([
	"com/bowdark/productsDemoApp/controller/BaseController" // Base Controller for back button navigation
], function(BaseController) {
	"use strict";
	
	return BaseController.extend("com.bowdark.productsDemoApp.controller.ListReport", {
		// When item is clicked, navigate to "ObjectReport" page
		handleListItemPress : function(oEvent){
			var oItem, oCtx;
			oItem = oEvent.getSource();
			oCtx = oItem.getBindingContext();
			
			// Get the ProductID from the binding context
			this.getRouter().navTo("ObjectReport", {
				productId : oCtx.getObject("ProductID")
			});
		},
		
		// Deletes a product from the Northwind oData
		deleteProduct : function (oEvent) {
			// use the product id to delete it from the database
			var oList, oItem, sPath;
			oList = oEvent.getSource();
			oItem = oEvent.getParameter("listItem"); // For list, listItem directly refers to what was clicked
			sPath = oItem.getBindingContext().getPath();
			
			console.log("[BaseController]oItem ProductID: " + sPath);

			// after deletion put the focus back to the list
			//oList.attachEventOnce("updateFinished", oList.focus, oList);

			// send a delete request to the odata service
			//this.oProductModel.remove(sPath);
		}
	});
});