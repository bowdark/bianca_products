sap.ui.define([
	"com/bowdark/productsDemoApp/controller/BaseController" // Base Controller for back button navigation
], function(BaseController) {
	"use strict";
	
	return BaseController.extend("com.bowdark.productsDemoApp.controller.Master", {
		// Display the "notFound" target without changing the hash
		onDisplayNotFound : function (oEvent) {
			this.getRouter().getTargets().display("notFound", {
				fromTarget : "master"
			});
		},
		
		// Navigate to the "ListReport" view
		onNavListReport : function(oEvent) {
			this.getRouter().navTo("ListReport");
		},
		
		// If input is given, go to "ObjectReport"
		onProductIdInput : function(oEvent) {
			var oItem = this.getView().byId("productIdInput");
			
			if (oItem !== null) {
				this.getRouter().navTo("ObjectReport", {
					productId : oItem.getValue()
				});
				
				// Clears the value in "productIdInput"
				oItem.setValue("");
			}
		}
	});
});