sap.ui.define([
	"com/bowdark/productsDemoApp/controller/BaseController" // Base Controller for back button navigation
], function(BaseController) {
	"use strict";
	
	return BaseController.extend("com.bowdark.productsDemoApp.controller.ObjectReport", {
		// MAKE SURE IT'S onInit OR THE CONTROLLER WON'T REGISTER THE ROUTER AND RUN THE ROUTING FUNCTIONS
		onInit : function () {
			var oRouter = this.getRouter();
			oRouter.getRoute("ObjectReport").attachMatched(this._onRouteMatched, this);
		},
		
		// If there is data bound, go to ObjectPage with the associated product information
		_onRouteMatched: function (oEvent) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();
			
			// Use the elements (ProductID from Products oData) taken from the previous page to populate the page.
			oView.bindElement({
				path : "/Products(" +  oArgs.productId + ")",
				parameters: {
					expand: "Supplier,Category"
				},
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function (oEvent) { // Busy indicator while data requested (Y/N)
						oView.setBusy(true);
					},
					dataReceived: function (oEvent) {
						oView.setBusy(false);
					}
				}
			});
		},
		
		// In case of no data binding, go to NotFound
		_onBindingChange: function (oEvent) {
			if (!this.getView().getBindingContext()) {
				this.getRouter().getTargets().display("notFound");
			}
		},
		
		// Overload the BaseController deleteProduct
		deleteProduct : function (oEvent) {
			// Get the ProductName from the text view
			var oView = this.getView();
			var oProperty = oView.getBindingContext().getProperty();
			var oModel = this.getView().getModel();
			
			// Confirm the user wants to delete the item
			jQuery.sap.require("sap.m.MessageBox");
			sap.m.MessageBox.confirm(
				"Confirm deletion of " + oProperty["ProductName"] + " from the Product List?", {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Delete Product",
					actions: [sap.m.MessageBox.Action.DELETE, sap.m.MessageBox.Action.CANCEL],
					initialFocus: sap.m.MessageBox.Action.CANCEL,
					onClose: function(oAction) { 
						// If "delete", then erase data and exit to the previous screen 
						if (oAction === "DELETE") {
							// DELETE serviceRoot/Products(ProductID);
							oModel.remove("/Products(" + oProperty["ProductID"] + ")", null, function() {
								sap.m.MessageToast.show("Delete Operation successful");
								// oModel.refresh();
							}, function() {
								sap.m.MessageToast.show("Delete Operation Failed");
							});
							
							// Return to the previous page
							var oHistory, sPreviousHash, oRouter;
							if (this._oData && this._oData.fromTarget) {
								this.getRouter().getTargets().display(this._oData.fromTarget);
								delete this._oData.fromTarget;
								return;
							}
							BaseController.prototype.onNavBack.apply(this, arguments);
						}
					}
				}
			);
		}
	});
});