sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History" // History library
], function (Controller, History) {
	"use strict";
	
	return Controller.extend("com.bowdark.productsDemoApp.controller.BaseController", {
		getRouter : function () {
			return sap.ui.core.UIComponent.getRouterFor(this); 
		},
		
		// Get the previous URL hash and return to it upon button press.
		//    If not available, then go to the Master page.
		onNavBack : function (oEvent) {
			var oHistory, sPreviousHash;
			
			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			}
			else {
				this.getRouter().navTo("appMaster", {}, true /*meaning no history*/);
			}
		},
		
		// Adds a product to the Northwind oData
		createProduct : function () {
			// idk how we're going to get the next available "ProductID" but dang shall we try
			// this is required since there is no direct access to the box's icons like MessageBox.Icon.WARNING
			jQuery.sap.require("sap.m.MessageBox");
			sap.m.MessageBox.show(
				"Boy don't you wish you actually added something.", {
					icon: sap.m.MessageBox.Icon.INFORMATION,
					title: "WE'RE ADDING YA HECKS",
					actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
					onClose: function(oAction) { /* do something */ }
				}
			);
		},
		
		// Updates a product on the Northwind oData
		updateProduct : function () {
			// update in here
			
			// Write your http code for updating the stuffs in here
			jQuery.sap.require("sap.m.MessageBox");
			sap.m.MessageBox.show(
				"Boy don't you wish you actually updated something.", {
					icon: sap.m.MessageBox.Icon.INFORMATION,
					title: "UPDATE",
					actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
					onClose: function(oAction) { /* do something */ }
				}
			);
		}
	});
});